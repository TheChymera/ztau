from samri.utilities import bids_autofind_df, ordered_structures
from samri.analysis.fc import correlation_matrix
from samri.fetch.local import summary_atlas

bids_df = bids_autofind_df('~/.scratch/ztau/preprocessing/',
	path_template='sub-{{subject}}/ses-{{session}}/func/'\
		'sub-{{subject}}_ses-{{session}}_task-rest_acq-EPI_run-{{run}}.nii.gz',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/func/'\
		'.*?_acq-(?P<acquisition>.+)_run-(?P<run>.+)\.nii\.gz',
	)

bids_df['out_path'] = bids_df['path'].str.replace('preprocessing','summary_correlations')
bids_df['out_path'] = bids_df['out_path'].str.replace('.nii.gz','_correlation.csv')
bids_df['out_path'] = bids_df['out_path'].str.replace('/func','')

mapping='/usr/share/mouse-brain-atlases/dsurqe_labels.csv'
atlas='/usr/share/mouse-brain-atlases/dsurqec_40micron_labels.nii'
summary={
	1:{
		'structure':'Hippocampus',
		'summarize':[
			'CA10r',
			'LMol',
			'CA1Rad',
			'CA2Py',
			'CA20r',
			'CA2Rad',
			'CA3Py Inner',
			'CA3Py Outer',
			'CA30r',
			'CA3Rad',
			'SLu',
			'MoDG right',
			'GrDG',
			'PoDG',
			'CA1Py',
			],
		'laterality':'right',
		},
	2:{
		'structure':'Hippocampus',
		'summarize':[
			'CA10r',
			'LMol',
			'CA1Rad',
			'CA2Py',
			'CA20r',
			'CA2Rad',
			'CA3Py Inner',
			'CA3Py Outer',
			'CA30r',
			'CA3Rad',
			'SLu',
			'MoDG right',
			'GrDG',
			'PoDG',
			'CA1Py',
			],
		'laterality':'left',
		},
	3:{
		'structure':'Enthorhinal Cortex',
		'summarize':[
			'Dorsal intermediate entorhinal cortex',
			'Dorsolateral entorhinal cortex',
			'Medial entorhinal cortex',
			'Caudomedial entorhinal cortex',
			'Ventral intermediate entorhinal cortex',
			],
		'laterality':'right',
		},
	4:{
		'structure':'Enthorhinal Cortex',
		'summarize':[
			'Dorsal intermediate entorhinal cortex',
			'Dorsolateral entorhinal cortex',
			'Medial entorhinal cortex',
			'Caudomedial entorhinal cortex',
			'Ventral intermediate entorhinal cortex',
			],
		'laterality':'left',
		},
	}

new_atlas, new_mapping = summary_atlas(atlas,mapping,
	summary=summary,
	)
structure_names = ordered_structures(new_atlas,new_mapping)

for index, row in bids_df.iterrows():
	correlation_matrix(row['path'],
		atlas=new_atlas,
		save_as=row['out_path'],
		structure_names=structure_names,
		)
