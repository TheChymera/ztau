import pandas as pd
import matplotlib.pyplot as plt
import samri.plotting.maps as maps
from os import path
import seaborn as sns

# Style elements
palette=["#E09000","#56B4E9"]

data_dir = path.join(path.dirname(path.realpath(__file__)),"../data")
data_path = path.join(data_dir,'trajectories.csv')
df = pd.read_csv(data_path)

# definitions for the axes
#left, width = 0.06, 0.9
#bottom, height = 0, 0.9

#session_coordinates = [left, bottom, width, height]
#roi_coordinates = [left+0.53, bottom+0.01, 0.36, 0.24]

fig = plt.figure(1)
#ax1 = plt.axes(session_coordinates)
sns.pointplot(
	x='session',
	y='Hippocampus (R)/Enthorhinal Cortex (R)',
	units='subject',
	data=df,
	hue='disease_vector',
	dodge=True,
	palette=palette,
#	ax=ax1,
	ci=95,
	)

#ax1.xaxis.tick_top()
#ax1.xaxis.set_label_position('top')
#
#ax2 = plt.axes(roi_coordinates)
#maps.atlas_label("/usr/share/mouse-brain-atlases/dsurqec_40micron_labels.nii",
#        scale=0.5,
#        color="#E09000",
#        ax=ax2,
#        annotate=False,
#        mapping="/usr/share/mouse-brain-atlases/dsurqe_labels.csv",
#        label_names=["cortex","Cortex"],
#        alpha=0.8,
#        )
