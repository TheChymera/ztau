import pandas as pd
from itertools import product
from samri.utilities import bids_autofind_df

bids_df = bids_autofind_df('~/.scratch/ztau/summary_correlations/',
	path_template='sub-{{subject}}/ses-{{session}}/'\
		'sub-{{subject}}_ses-{{session}}_task-rest_acq-EPI_run-{{run}}_correlation.csv',
	match_regex='.+sub-(?P<sub>.+)/ses-(?P<ses>.+)/'\
		'.*?_acq-(?P<acquisition>.+)_run-(?P<run>.+)_correlation\.csv',
	)
participants = '/home/chymera/participants.tsv'
participants_df = pd.read_csv(participants, sep='\t')

for index, row in bids_df.iterrows():
	path = row['path']
	df = pd.read_csv(path, index_col=0)
	columns = df.columns
	done_combinations = []
	for a, b in product (columns,columns):
		if a == b:
			continue
		done = False
		for done_combination in done_combinations:
			if [b,a] == done_combination:
				done = True
		if done:
			continue
		val = df[a].ix[b]
		name = '{}/{}'.format(a,b)
		bids_df.loc[bids_df['path']==path,name] = val
		done_combinations.append([a,b])

# Column types can differ depending on the dataframe source
participants_df = participants_df.astype({'subject': 'str'})
bids_df = bids_df.astype({'subject': 'str'})
bids_df = pd.merge(bids_df, participants_df, on='subject', how='inner')

bids_df.to_csv('../data/trajectories.csv')
