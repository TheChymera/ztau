#!/usr/bin/env bash

if [ ! -d ~/.scratch ]; then
	echo "You seem to be lacking a ~/.scratch/ directory."
	echo "We need this directory in order to process the data, and it needs to be on a volume with 200GB+ space."
	echo "You can simply symlink to a location you would like this to happen (and then re-run this script):
		ln -s /where/you/want/it ~/.scratch"
	exit 1
fi

if [ ! -d ~/.scratch/ztau/bids ]; then
	if [ -d '/usr/share/ztau_bidsdata' ]; then
		[ -d ~/.scratch/ztau ] || mkdir ~/.scratch/ztau
		ln -s '/usr/share/ztau_bidsdata' ~/.scratch/ztau/bids
	else
		echo "No IRSABI BIDS data distribution found, processing from scanner IRSABI data:"
		python make_bids.py
	fi
fi

#SAMRI bru2bids -o ~/.scratch/ztau/ -f '{"acquisition":["EPI"]}' -s '{"acquisition":["TurboRARE"]}' /mnt/data/ztau/
python preprocess.py
python manual_overview.py
python connectivity.py
python trajectory.py
