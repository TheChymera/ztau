from samri.pipelines.preprocess import generic

scratch_dir = '~/.scratch/ztau'

# Preprocess drinking water data:
bids_base = '{}/bids'.format(scratch_dir)
generic(bids_base,
        '/usr/share/mouse-brain-atlases/dsurqec_200micron.nii',
        #functional_blur_xy=0.25,
        registration_mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
        #sessions=['ofM','ofMaF','ofMcF1','ofMcF2','ofMpF'],
        functional_match={'acquisition':['EPI'],},
        structural_match={'acquisition':['TurboRARE'],},
        out_base=scratch_dir,
        workflow_name='preprocessing',
        )

