import numpy as np
import pandas as pd
from statsmodels.multivariate.manova import MANOVA

df = pd.read_csv('data/trajectories.csv')
columns = df.columns
value_columns = [i for i in columns if '/' in i]

# convert exog to dummy
# https://github.com/statsmodels/statsmodels/issues/6155#issuecomment-530637971
exog = (np.asarray(df['session'])[:,None] == np.unique(df['session'])).astype(np.float64)
endog = np.asarray(df[value_columns])

model = MANOVA(endog=endog, exog=exog)
res = model.mv_test()
print(res.summary())
